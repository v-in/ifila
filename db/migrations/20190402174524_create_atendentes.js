exports.up = function(knex, Promise) {
  return knex.schema.createTable("Atendentes", table => {
    table.increments("id");
    table
      .string("email")
      .notNull()
      .unique();
    table
      .string("cpf")
      .notNull()
      .unique();
    table
      .integer("idPessoa")
      .notNull()
      .references("id")
      .inTable("Pessoas");
    table
      .integer("idEstabelecimento")
      .notNull()
      .references("id")
      .inTable("Estabelecimentos");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Atendentes");
};
