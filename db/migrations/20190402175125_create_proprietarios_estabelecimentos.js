exports.up = function(knex, Promise) {
  return knex.schema.createTable("Proprietario_Estabelecimento", table => {
    table
      .integer("idProprietario")
      .notNull()
      .unique()
      .references("id")
      .inTable("Usuarios");
    table
      .integer("idEstabelecimento")
      .notNull()
      .unique()
      .references("id")
      .inTable("Estabelecimentos");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Proprietario_Estabelecimento");
};
