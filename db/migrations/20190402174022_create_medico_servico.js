exports.up = function(knex, Promise) {
  return knex.schema.createTable("Medico_Servico", table => {
    table
      .integer("idMedico")
      .notNull()
      .references("id")
      .inTable("Medicos");
    table
      .integer("idServico")
      .notNull()
      .references("id")
      .inTable("Servicos");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Medico_Servico");
};
