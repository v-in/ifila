exports.up = function(knex, Promise) {
  return knex.schema.createTable("Estabelecimentos", table => {
    table.increments("id").primary();
    table.string("email");
    table.string("logradouro");
    table.string("bairro");
    table.string("estado");
    table.string("cidade");
    table.string("pais");
    table.string("numero");
    table.string("referencia");
    table.float("latitude");
    table.float("longitude");
    table
      .string("cnpj")
      .notNull()
      .unique();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Estabelecimentos");
};
