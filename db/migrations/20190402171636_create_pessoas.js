exports.up = function(knex, Promise) {
  return knex.schema.createTable("Pessoas", table => {
    table.increments("id").primary();
    table.string("email");
    table.string("nome");
    table.string("sobrenome");
    table.string("senha");
    table.integer("genero");
    table.dateTime("nascimento");
    table.binary("foto");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Pessoas");
};
