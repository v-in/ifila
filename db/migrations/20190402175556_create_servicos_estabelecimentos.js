exports.up = function(knex, Promise) {
  return knex.schema.createTable("Servicos_Estabelecimentos", table => {
    table.increments("id").primary();
    table
      .integer("idServico")
      .notNull()
      .references("id")
      .inTable("Servicos");
    table
      .integer("idEstabelecimento")
      .notNull()
      .references("id")
      .inTable("Estabelecimentos");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Servicos_Estabelecimentos");
};
