exports.up = function(knex, Promise) {
  return knex.schema.createTable("Servicos", table => {
    table.increments("id").primary();
    table.string("descricao").notNull();
    table.float("valor").notNull();
    table.integer("duracao").notNull();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Servicos");
};
