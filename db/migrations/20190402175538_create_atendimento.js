exports.up = function(knex, Promise) {
  return knex.schema.createTable("Atendimentos", table => {
    table.increments("id").primary();
    table.integer("status");
    table
      .integer("idUsuario")
      .notNull()
      .references("id")
      .inTable("Usuarios");
    table
      .integer("idHorario")
      .notNull()
      .references("id")
      .inTable("HorariosDisponiveis");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Atendimentos");
};
