exports.up = function(knex, Promise) {
  return knex.schema.createTable("Proprietarios", table => {
    table.increments("id").primary();
    table
      .string("cpf")
      .notNull()
      .unique();
    table
      .string("email")
      .notNull()
      .unique();
    table
      .integer("idPessoa")
      .notNull()
      .references("id")
      .inTable("Pessoas");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Proprietarios");
};
