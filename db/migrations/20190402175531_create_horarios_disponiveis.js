exports.up = function(knex, Promise) {
  return knex.schema.createTable("HorariosDisponiveis", table => {
    table.increments("id").primary();
    table.time("inicio");
    table.time("fim");
    table
      .integer("idServico")
      .notNull()
      .references("id")
      .inTable("Servicos");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("HorariosDisponiveis");
};
