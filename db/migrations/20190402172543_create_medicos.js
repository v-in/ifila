exports.up = function(knex, Promise) {
  return knex.schema.createTable("Medicos", table => {
    table.increments("id").primary();
    table.string("CRM").notNull();
    table.string("especializacao").notNull();
    table
      .string("email")
      .notNull()
      .unique();
    table
      .string("cpf")
      .notNull()
      .unique();
    table
      .integer("idPessoa")
      .notNull()
      .references("id")
      .inTable("Pessoas");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists("Medicos");
};
