exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex("Pessoas")
    .del()
    .then(function() {
      // Inserts seed entries
      return knex("Pessoas").insert([
        {
          id: 1,
          nome: "Reynold",
          sobrenome: "Cridland",
          email: "rcridland0@biglobe.ne.jp",
          senha: "IUyV1sB7Xxq7",
          nascimento: "2018-06-19T22:18:50Z",
          foto: null,
          genero: 3
        },
        {
          id: 2,
          nome: "Stephie",
          sobrenome: "Philipps",
          email: "sphilipps1@paypal.com",
          senha: "33447wc",
          nascimento: "2018-07-22T13:23:44Z",
          foto: null,
          genero: 1
        },
        {
          id: 3,
          nome: "Kerstin",
          sobrenome: "Cowwell",
          email: "kcowwell2@fotki.com",
          senha: "5jATrQ",
          nascimento: "2019-02-17T22:49:56Z",
          foto: null,
          genero: 4
        },
        {
          id: 4,
          nome: "Casar",
          sobrenome: "Swains",
          email: "cswains3@cpanel.net",
          senha: "BJs6HFLHC8",
          nascimento: "2018-04-26T04:55:54Z",
          foto: null,
          genero: 4
        },
        {
          id: 5,
          nome: "Raimundo",
          sobrenome: "Hurley",
          email: "rhurley4@vistaprint.com",
          senha: "BsFUnqa",
          nascimento: "2018-10-15T23:36:43Z",
          foto: null,
          genero: 3
        },
        {
          id: 6,
          nome: "Katerina",
          sobrenome: "Ferrick",
          email: "kferrick5@cisco.com",
          senha: "cT8gqfPgA9",
          nascimento: "2018-12-07T00:57:22Z",
          foto: null,
          genero: 3
        },
        {
          id: 7,
          nome: "Berkeley",
          sobrenome: "McDonnell",
          email: "bmcdonnell6@xrea.com",
          senha: "wkaBfyZHJc",
          nascimento: "2018-07-06T09:58:08Z",
          foto: null,
          genero: 2
        },
        {
          id: 8,
          nome: "Henrik",
          sobrenome: "Poncet",
          email: "hponcet7@naver.com",
          senha: "JmEUag",
          nascimento: "2019-03-30T19:41:45Z",
          foto: null,
          genero: 3
        },
        {
          id: 9,
          nome: "Arte",
          sobrenome: "Greenstead",
          email: "agreenstead8@yellowpages.com",
          senha: "Rsv6XaGIsb",
          nascimento: "2018-04-24T03:46:59Z",
          foto: null,
          genero: 3
        },
        {
          id: 10,
          nome: "Luisa",
          sobrenome: "McOwan",
          email: "lmcowan9@goo.gl",
          senha: "I0wGFsPD7",
          nascimento: "2019-01-27T02:24:52Z",
          foto: null,
          genero: 3
        }
      ]);
    });
};
